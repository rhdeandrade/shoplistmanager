angular.module('Authentication', []);
angular.module('ShoplistControllers',[]);

var shopListApp = angular.module('shopListApp', ['ngRoute', 'ngCookies','ShoplistControllers','Authentication','ngFileUpload']);

shopListApp.config(['$routeProvider',
    function($routeProvider) {
		$routeProvider.
		when('/newshoplist', {
			templateUrl: 'app/partials/newshoplist.html',
			controller: 'ShopListController'
		}).
		when("/newshoplist/:listId", {
			templateUrl: 'app/partials/newshoplistitems.html',
			controller: 'ShopListItemsController'
		}).
		when('/login', {
			templateUrl: 'app/partials/authentication/login.html',
			controller: 'LoginController'
		}).
		when("/", {
			templateUrl: 'app/partials/home.html',
			controller: 'HomeController'
		}).
		otherwise({
			redirectTo: '/login'
		});
	}]).run(['$rootScope', '$location', '$cookieStore', '$http', function ($rootScope, $location, $cookieStore, $http) {
		$rootScope.errors = [];
		$rootScope.warnings = [];
		$rootScope.successes = [];
		
		$rootScope.resetMessage = function() {
			$rootScope.errors = [];
			$rootScope.warnings = [];
			$rootScope.successes = [];
		}
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        	$rootScope.errors = [];
    		$rootScope.warnings = [];
    		$rootScope.successes = [];
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);


//shopListApp.controller('ShopListController', function ($scope, $http) {
//	
//});