var ShoplistControllers = angular.module('ShoplistControllers', []);

ShoplistControllers.controller('ShopListController', ['$scope', '$rootScope','$http','$route', '$location', function($scope, $rootScope, $http, $route, $location) {
	$scope.submitShoplistName = function() {
		if ($scope.shoplistName) {
			var data = {};
			data['listName'] = $scope.shoplistName;
			data['id'] = null;
			$http.post('/ShopListREST/ShopListREST/shoplist/saveList', JSON.stringify(data)).success(function(data) {
				$location.path("/newshoplist/" + data.listId);
				$rootScope.successes.push({"name" : "Sucesso", "message" : "Sucesso ao salvar a nova lista"});
			}).error(function(data){
				$rootScope.errors.push({"name" : "Erro ao salvar", "message" : "Não foi possível salvar a lista"});
			});
	    
		}
	};
}]);


ShoplistControllers.controller('ShopListItemsController', ['$scope', '$rootScope','$http','$route','$routeParams','$location','Upload', '$timeout',
                                                           function($scope, $rootScope, $http, $route, $routeParams, $location, Upload, $timeout) {
	$scope.listId = ($routeParams.listId || -1);
	$scope.shoplist = {};
	$scope.items = [];
	$scope.itemsForm = [];
	
	$http.get('/ShopListREST/ShopListREST/shoplist/' + $scope.listId).success(function(data) {
		if (data['id']) {
			$scope.shoplist.id = data['id'];
			$scope.shoplist.listName = data['listName'];
		}
		else {
			$rootScope.warnings.push({"name" : "Não encontrado", "message" : "Não foi encontrar a lista"});
			$location.path("/");
		}
	}).error(function(data) {
		$rootScope.errors.push({"name" : "Erro ao buscar", "message" : "Não foi possível buscar a lista"});
	});
	
	$http.get('/ShopListREST/ShopListREST/shoplist/' + $scope.listId + '/listItems').success(function(data) {
		$scope.items = data;
		if ($scope.items.length == 0) {
			$scope.addItem();
		}
		else {
			// file property to collection
			for (i = 0; i < $scope.items.length; i++ ) {
				$scope.items[i]['file'] = null;
			}
		}
	}).error(function(data) {
		$rootScope.errors.push({"name" : "Erro buscar", "message" : "Não foi possível buscar os items da lista"});
	});
	
	$scope.addItem = function() {
		
		var item = {"bought" : false, "price" : 0.0,"quantity" : 0,"name" : null, "id" : null, "listId" : null, "fileName" : "", "file" : null};
		$scope.items.push(item);
	};
	
	$scope.removeItem = function(item) {
		if (item.id) {
			$http.delete('/ShopListREST/ShopListREST/shoplist/shopListItem/' + item.id).success(function(data){
				$rootScope.successes.push({"name" : "Item removido", "message" : "Item removido com sucesso"});
			});
		}
		$scope.items.splice($scope.items.indexOf(item), 1);
	};
	
	$scope.submitItem = function(item) {
        $http.post('/ShopListREST/ShopListREST/shoplist/' + $scope.shoplist.id + '/saveListItem', angular.toJson(item)).success(function(data) {
			//
		}).error(function(data){
			$rootScope.errors.push({"name" : "Erro ao salvar", "message" : "Não foi possível salvar os itens"});
		});
	}
	
	$scope.submitList = function() {
		
		for(i = 0; i < $scope.items.length; i++) {
			var item = $scope.items[i];
			var file = item.file;
			
			if (file != null && !file.$error) {
	            file.upload = Upload.upload({
	                url: '/ShopListREST/ShopListREST/shoplist/fileupload',
	                file: file
	            });
	
	            file.upload.then(function (response) {
	                $timeout(function () {
//		                    file.result = response.data;
	                    item.fileName = response.data['file_name'];
	                	var itemToSave = angular.copy(item);
	                    delete itemToSave['file'];
	                	$scope.submitItem(itemToSave);
	                });
	            }, function (response) {
	                if (response.status > 0)
	                    $scope.errorMsg = response.status + ': ' + response.data;
	            });
	
	            file.upload.progress(function (evt) {
	            	if (file)
	            		file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	            });
			}
			else {
				var itemToSave = angular.copy(item);
                delete itemToSave['file'];
                
                $scope.submitItem(itemToSave);
			}
		}
		
		
		$rootScope.successes.push({"name" : "Salvo com sucesso", "message" : "Seus itens foram salvos com sucesso."});
			
	};
	
	$scope.getFile = function(fileName) {
		if (fileName) {
			return '/ShopListREST/ShopListREST/shoplist/images/' + fileName;
		}
		else {
			return null;
		}
	};
	
	//File Uploader
	$scope.uploadFiles = function(file, item) {
		item.fileName = file.name;
		item.file = file;  
    };
    
}]);

ShoplistControllers.controller('HomeController',['$scope', '$rootScope', '$http', '$location', 'AuthenticationService', function ($scope, $rootScope, $http, $location, AuthenticationService){
	$scope.shoplists = [];

	$http.get('/ShopListREST/ShopListREST/shoplist/').success(function(data) {
		$scope.shoplists = data;
	}).error(function(data) {
		$rootScope.errors.push({"name" : "Erro ao buscar", "message" : "Não foi possível buscar suas listas"});
	});
	
	$scope.deleteShoplist = function(shoplist) {
		$http.delete('/ShopListREST/ShopListREST/shoplist/' + shoplist.id).success(function(data){
			$rootScope.successes.push({"name" : "Item removido", "message" : "Item removido com sucesso"});
			$scope.shoplists.splice($scope.shoplists.indexOf(shoplist), 1);
		}).error(function(data){
			$rootScope.errors.push({"name" : "Erro ao remover", "message" : "Não foi possível remover esta lista"});
		});
	};
}]); 

ShoplistControllers.controller('LoginController',['$scope', '$rootScope', '$location', 'AuthenticationService', function ($scope, $rootScope, $location, AuthenticationService) {
	// reset login status
	AuthenticationService.ClearCredentials();

	$scope.login = function () {
		$scope.dataLoading = true;
		AuthenticationService.Login($scope.username, $scope.password, function (response) {
			if (response.success) {
				AuthenticationService.SetCredentials($scope.username, $scope.password);
				$location.path('/');
			} else {
				$scope.error = response.message;
				$scope.dataLoading = false;
			}
		});
	};
}]);