package br.com.raphael.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.raphael.model.Item;
import br.com.raphael.model.Shoplist;

public class PersistenceManager {
	private EntityManagerFactory emfactory;

	public PersistenceManager() {
		emfactory = Persistence.createEntityManagerFactory("SHOPLIST_JPA");
//		System.out.println("Opened database successfully");
	}

	public Shoplist createShopList(Shoplist shoplist) {
		EntityManager entitymanager = emfactory.createEntityManager( );
		entitymanager.getTransaction().begin( );
		entitymanager.persist(shoplist);
		entitymanager.flush();
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
		return shoplist;
	}
	
	public Shoplist getShopList(int id) {
		EntityManager entitymanager = emfactory.createEntityManager( );
		entitymanager.getTransaction().begin( );

		Shoplist s = new Shoplist();
		
		s = entitymanager.find(Shoplist.class, id);
		
		entitymanager.close();
		
		return s;
	}
	
	public List<Shoplist> getAllShoplist() {
		List<Shoplist> shoplists = new ArrayList<>();
		
		EntityManager entityManager = emfactory.createEntityManager();
		
		List<Object[]> result =  (List<Object[]>) entityManager.createQuery("select id, listName from Shoplist order by id desc").getResultList();
		
		for (Object[] objects : result) {
			Shoplist s = new Shoplist();
			s.setId((Integer) objects[0]);
			s.setListName((String) objects[1]);
			
			shoplists.add(s);
		}
		
		return shoplists;
	}
	
	public List<Item> getItemsbyListId(int listId) {
		List<Item> items = new ArrayList<>();
		
		EntityManager entityManager = emfactory.createEntityManager();
		
		List<Object[]> result =  (List<Object[]>) entityManager.createQuery("select id, listId, name, bought, price, quantity, fileName from Item where listId = :listId ")
				.setParameter("listId", listId).getResultList();
		
		for (Object[] objects : result) {
			Item i = new Item();
			i.setId((Integer) objects[0]);
			i.setListId((Integer) objects[1]);
			i.setName((String) objects[2]);
			i.setBought((Boolean) objects[3]);
			i.setPrice((Float) objects[4]);
			i.setQuantity((Integer) objects[5]);
			i.setFileName((String) objects[6]);
			
			items.add(i);
		}
		
		return items;
	}
	
	public Item createShopListItems(Item item) {
		EntityManager entitymanager = emfactory.createEntityManager( );
		entitymanager.getTransaction().begin( );
		entitymanager.merge(item);
		entitymanager.flush();
		entitymanager.getTransaction().commit();
		entitymanager.close();
		
		return item;
	}
	
	public void removeShoplist(int id) {
		EntityManager entityManager = emfactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove((Shoplist)entityManager.find(Shoplist.class, id));
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	public void removeShoplistItem(int id) {
		EntityManager entityManager = emfactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove((Item)entityManager.find(Item.class, id));
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	public Item getShoplistItem(int id) {
		EntityManager entityManager = emfactory.createEntityManager();
		entityManager.getTransaction().begin();
		Item i = (Item)entityManager.find(Item.class, id);
		entityManager.getTransaction().commit();
		entityManager.close();
		
		return i;
	}
	
	@Override
	public void finalize() {
		emfactory.close();
	}
	
	
}

