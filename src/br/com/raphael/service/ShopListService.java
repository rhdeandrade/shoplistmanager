package br.com.raphael.service;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import br.com.raphael.model.Item;
import br.com.raphael.model.Shoplist;
import br.com.raphael.persistence.PersistenceManager;
import br.com.raphael.util.FileUtil;

@Path("/shoplist")
public class ShopListService {
	private static final String DIR = "uploadedFiles/";
	
	@GET
	@Produces("application/json")
	public List<Shoplist> getLists() {
		
		PersistenceManager p = new PersistenceManager();
		
		List<Shoplist> list = p.getAllShoplist();
		
		return list;
	}
	
	@GET
	@Path("/{listid}")
	@Produces("application/json")
	public Shoplist getListById(@PathParam("listid") String listId) {

		Shoplist list = new Shoplist();
		list.setId(Integer.parseInt(listId));
		
		PersistenceManager p = new PersistenceManager();
		list = p.getShopList(Integer.parseInt(listId));
		
		return list;
	}
	
	@DELETE
	@Path("/{listid}")
	public Response removeShoplist(@PathParam("listid") String listId) {
		System.out.println("Remover " + listId);
		
		PersistenceManager p = new PersistenceManager();
		Shoplist s = p.getShopList(Integer.parseInt(listId));
		
		List<Item> l = p.getItemsbyListId(s.getId());
		
		for (Item item : l) {
			File  objFile = new File(DIR + item.getFileName());
			if(objFile.exists())
			{
				objFile.delete();
			}
			p.removeShoplistItem(item.getId());
		}
		
		p.removeShoplist(s.getId());
		
		return Response.status(200).build();
	}
	
	@GET
	@Path("/{listid}/listItems")
	@Produces("application/json")
	public List<Item> getListItemsByListId(@PathParam("listid") String listId) {

		PersistenceManager p = new PersistenceManager();
		List<Item> items = p.getItemsbyListId(Integer.parseInt(listId));
		
		return items;
	}
	
	@POST
	@Path("/saveList")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveNewList(Shoplist list) {
		System.out.println(list.getListName() + " " + list.getId());
		PersistenceManager p = new PersistenceManager();
		list = p.createShopList(list);
		
		String result = "{ \"listId\": " + list.getId() + ", \"listName\" : \""+ list.getListName() + "\"}";
		return Response.status(200).entity(result).build();
	}
	
	@POST
	@Path("/{listid}/saveListItems")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveListItems(@PathParam("listid") String listId, List<Item> lst) {
		System.out.println(listId);
		System.out.println(lst.size());
		
		PersistenceManager p = new PersistenceManager();
		for (Item item : lst) {
			item.setListId(Integer.parseInt(listId));

			p.createShopListItems(item);
		}
		return Response.status(200).entity("{ \"ok\" : 1 }").build();
	}
	
	@POST
	@Path("/{listid}/saveListItem")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveListItem(@PathParam("listid") String listId, Item item) {
		
		PersistenceManager p = new PersistenceManager();
		item.setListId(Integer.parseInt(listId));

		p.createShopListItems(item);
		return Response.status(200).entity("{ \"ok\" : 1 }").build();
	}
	
	@DELETE
	@Path("/shopListItem/{id}")
	public Response removeShoplistItem(@PathParam("id") String id) {
		System.out.println("Remover " + id);
		
		PersistenceManager p = new PersistenceManager();
		Item i = p.getShoplistItem(Integer.parseInt(id));
		
		File  objFile = new File(DIR + i.getFileName());
		if(objFile.exists())
		{
			objFile.delete();
		}
		p.removeShoplistItem(i.getId());
		
		return Response.status(200).build();
	}
	
	
	@POST
	@Path("/fileupload")  //Your Path or URL to call this service
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@DefaultValue("true") @FormDataParam("enabled") boolean enabled,
								@FormDataParam("file") InputStream uploadedInputStream,
								@FormDataParam("file") FormDataContentDisposition fileDetail) {

		Date d = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_");
		String partialName = format.format(d) + (new Random().nextInt(1000) + 1000); 
		String originalName = fileDetail.getFileName();
		String fileName = partialName + originalName.substring(originalName.lastIndexOf("."), originalName.length());
		String uploadedFileLocation = DIR + fileName;
		
		// Verify if Path exists
		File dir = new File(DIR);
		if (!dir.exists())
			dir.mkdirs();
		
		// save it
		File  objFile = new File(uploadedFileLocation);
		if(objFile.exists())
		{
			objFile.delete();
		}
		FileUtil.saveToFile(uploadedInputStream, uploadedFileLocation);

		String output = "{ \"file_name\" : \"" + fileName + "\"}";

		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/images/{image}")
	@Produces("image/*")
	public Response getImage(@PathParam("image") String image) {

		File f = new File(DIR + image);


		String mt = new MimetypesFileTypeMap().getContentType(f);
		return Response.ok(f, mt).build();
	}
}
	
