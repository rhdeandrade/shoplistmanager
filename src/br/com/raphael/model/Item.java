package br.com.raphael.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ITEMS")
@SequenceGenerator(name = "items_id_seq", sequenceName = "items_id_seq", allocationSize=1)
public class Item {
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "QUANTITY")
	private int quantity;
	
	@Column(name = "BOUGHT")
	private boolean bought;
	
	@Column(name = "PRICE")
	private float price;
	
	@Column(name = "SHOPLIST_ID")
	private int listId;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="items_id_seq")
	private int id;
	
	public Item() {
		
	}

	public String getName() {
		return name;
	}

	public Item setName(String name) {
		this.name = name;
		return this;
	}

	public int getQuantity() {
		return quantity;
	}

	public Item setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	public boolean isBought() {
		return bought;
	}

	public Item setBought(boolean bought) {
		this.bought = bought;
		return this;
	}

	public float getPrice() {
		return price;
	}

	public Item setPrice(float price) {
		this.price = price;
		return this;
	}


	public int getListId() {
		return listId;
	}

	public void setListId(int listId) {
		this.listId = listId;
	}
	
	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
