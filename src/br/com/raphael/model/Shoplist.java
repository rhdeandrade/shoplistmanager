package br.com.raphael.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SHOPLISTS")
@SequenceGenerator(name = "shoplists_id_seq", sequenceName = "shoplists_id_seq", allocationSize=1)
public class Shoplist {
	
	@Column(name = "LIST_NAME")
	private String listName;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="shoplists_id_seq")
	private int id;
	
	public Shoplist() {

	}
	
	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	

	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
